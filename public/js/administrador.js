
function myFunction(tipo) {
  var quantidade = document.getElementsByClassName("jumbotron").length;

  for (var i=0;i<quantidade;i++){
    if (i==tipo){
      (document.getElementsByClassName("jumbotron"))[i].style.display = 'flex'
    } else {
      (document.getElementsByClassName("jumbotron"))[i].style.display = 'none'
    }
  }
}

function requisicoes(numeroFomulario) {
  var frm=$('#form'+numeroFomulario);
  // var uploadFile = document.getElementById ("realInput");
  //          if (uploadFile.value.length == 0) {
  //              alert ("Please specify the path to the file to upload!");
  //              return;
  //          }

  frm.submit(function(e) {
    e.preventDefault();
       $.ajax({
           type: frm.attr('method'),
           url: frm.attr('action'),
           enctype: 'multipart/form-data',
           processData: false,  // Important!
           cache: false,
           data: frm.serialize(),
           success: function (data) {
              alert("Sucesso!");
           },
          error: function (e) {
             console.log(e);
           },
       });
  });
}

function arquivo() {
const realInput = document.getElementById('realInput');
const fileInfo = document.querySelector('.fileInfo');

realInput.addEventListener('change', () => {
  const name = realInput.value.split(/\\|\//).pop();
  const truncated = name.length > 50
    ? name.substr(name.length - 50)
    : name;

  fileInfo.innerHTML = truncated;
});
}
