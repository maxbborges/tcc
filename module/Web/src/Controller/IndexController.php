<?php

namespace Web\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\ServiceManager\ServiceManager;
use \Zend\Http\Response;
use Zend\Http\Client as HttpClient;

use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Response\QrCodeResponse;


class IndexController extends AbstractActionController
{
    public function indexAction()
    {
      $parametros = $this->params()->fromQuery();
      $client = new HttpClient();
      $client->setAdapter('Zend\Http\Client\Adapter\Curl');
      $tabelas = 'divulgacao';

      $parametros = [
        'tabela'=>$tabelas,
        'id'=>'*',
        'tipo'=>'cliente',
      ];

      $response = $client
        ->setUri('http://localhost:80'.$this->getRequest()->getBaseUrl().'/restful/')
        ->setMethod('GET')
        ->setParameterGET($parametros)
        ->send();

      $response = json_decode($response->getBody());
      return new ViewModel(['response'=>(array)$response[rand(0,(count((array)$response))-1)]]);
    }

    public function qrAction()
    {
      $link = $this->params()->fromRoute('dado');
      $link = base64_decode($link);
      $qrCode = new QrCode($link);
      $qrCode->setSize(100);

      header('Content-Type: '.$qrCode->getContentType());
      echo $qrCode->writeString();
      return True;
    }
}
