<?php

namespace Api\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\Client as HttpClient;

class RequisicoesController extends AbstractActionController
{
    public function indexAction()
    {
        $client = new HttpClient();
        $client->setAdapter('Zend\Http\Client\Adapter\Curl');

        $parametros = $this->params()->fromQuery();

        if (count($parametros)==0){
          $parametros = $_POST;
        }

        $client->setUri('http://localhost:80/restful');

        if (isset($_GET['method'])){
          $method = $parametros['method'];
          switch($method) {
              case 'get' :
                  $client->setMethod('GET');
                  $client->setParameterGET(array('id'=>$parametros));
                  break;
              case 'get-list' :
                  $client->setMethod('GET');
                  break;
              case 'create' :
                  $client->setMethod('POST');
                  $client->setParameterPOST($parametros);
                  break;
              case 'update' :
                  $client->setMethod('PUT');
                  $client->setParameterGET(array('id'=>$parametros));
              case 'delete' :
                  $client->setMethod('delete');
                  $client->setParameterGET(array('id'=>$parametros));
          }
        } else if($this->getRequest()->isGet()){
          if(isset($parametros['modificar'])){
            $client->setMethod('PUT');
          } else {
            $client->setMethod('GET');
          }
          $client->setParameterGET(array('id'=>$parametros));
        } else if($this->getRequest()->isPost()){
          $client->setMethod('POST');
          $client->setParameterPOST($parametros);
        } else if($this->getRequest()->isDelete()){
          $client->setMethod('delete');
          $client->setParameterGET(array('id'=>$parametros));
        } else if ($this->getRequest()->isPut()){
          $client->setMethod('PUT');
          $client->setParameterGET(array('id'=>$parametros));
        }



        $response = $client->send();
        if (!$response->isSuccess()) {
            $message = $response->getStatusCode() . ': ' . $response->getReasonPhrase();

            $response = $this->getResponse();
            $response->setContent($message);
            return $response;
        }
        $body = $response->getBody();

        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/hal+json');
        $response->setContent($body);

        return $response;
    }
}
