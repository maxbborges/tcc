<?php

namespace Api\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class RequisicoesController extends AbstractActionController
{
  // OBTEM TODOS OS DADOS DE UMA TABELA
  public function getAllAction(){
    $tabela = $this->params()->fromRoute('dado');
    if ($tabela==$tabela){
      require_once '/var/www/tcc-web/public/Connection.php';
      $sql = "select * from ".$tabela.";";
      $res = pg_exec($conn, $sql);

      while($linha = pg_fetch_array($res,$row = NULL, $result_type = PGSQL_ASSOC)){
        $vetor[] = $linha;
      }

      $response = $this->includeHeader()->setContent(json_encode($vetor));
      return $response;
    } else {
      // echo "Tabela ".strtoupper($tabela)." não foi encontrada";
      return new ViewModel();
    }
  }

  public function loginAction()
  {
    $matricula = $this->params()->fromPost('matricula');
    $senha = $this->params()->fromPost('senha');

    require_once 'public/Connection.php';
    $senhas = "select senha from aluno where matricula='".$matricula."';";
    $resposta = pg_fetch_array(pg_exec($conn, $senhas),$row = NULL, $result_type = PGSQL_ASSOC);

    if (md5($senha)==$resposta['senha']) {
        $resposta = "aluno";
    } else {
        $senhas = "select senha from professor where matricula='".$matricula."';";
        $resposta = pg_fetch_array(pg_exec($conn, $senhas), 0, $result_type = PGSQL_ASSOC);
        if (md5($senha)==$resposta['senha']) {
            $resposta = "professor";
        } else {
            $resposta = false;
        }
    }

    $response = $this->includeHeader()->setContent(json_encode($resposta));
    return $response;
  }

  // OBTEM APENAS 1 ALUNO ESPECIFICO
  public function getAlunoAction(){
    $id = $this->params()->fromRoute('dado');
    require_once '/var/www/tccnovo/public/Connection.php';
    $sql = "select * from aluno where matricula='".$id."';";
    $res = pg_exec($conn, $sql);
    $response = $this->includeHeader()->setContent(json_encode(pg_fetch_array($res,0,$result_type = PGSQL_ASSOC)) );
    return $response;
  }

  public function addAllAction(){
    $tabela = $this->params()->fromRoute('dado');
    require_once 'public/Connection.php';

    if ($tabela=='mensagem'){
      $matricula = $this->params()->fromPost('matricula');
      $id = $this->params()->fromPost('id');
      $mensagem = $this->params()->fromPost('mensagem');

      $sql = "insert into ".$tabela." (id_professor,id_turma,mensagem) values ('".$matricula."','".$id."','".$mensagem."');";
    }

    $res = pg_query($sql);

    if(pg_last_error($conn) == ""){
      $resposta = true;
    } else {
      $resposta = pg_last_error($conn);
    }

    $response = $this->includeHeader()->setContent(json_encode($resposta));
    return $response;
  }

  public function xAction()
  {
      $id = $this->params()->fromRoute('dado');
      require_once 'public/Connection.php';
      $sql = "select matriculado.id_turma, professor.nome as nome_professor, mensagem.mensagem from aluno inner join matriculado on aluno.matricula=matriculado.matricula_aluno inner join mensagem on matriculado.id_turma=mensagem.id_turma inner join professor on mensagem.id_professor=professor.matricula where aluno.matricula='".$id."' order by mensagem.id_mensagem desc;";
      $res = pg_exec($conn, $sql);

      while ($linha = pg_fetch_array($res, $row = null, $result_type = PGSQL_ASSOC)) {
          $vetor[] = array_map('htmlentities', $linha);
      }
      $response = $this->includeHeader()->setContent(json_encode($vetor));
      return $response;
  }




  // public function get($id){
  //   require_once 'public/Connection.php';
  //   $sql = "select * from menssagem where id_menssagem='".$id."';";
  //   $res = pg_exec($conn, $sql);
  //
  //   $response = $this->getResponseWithHeader()->setContent(json_encode(pg_fetch_array($res,0,$result_type = PGSQL_ASSOC)));
  //   return $response;
  // }
  //
  // public function getList(){
  //
  //   // Realiza a conexão com o banco e pega todas as colunas do banco.
  //   require_once 'public/Connection.php';
  //   $sql = "select * from menssagem order by id_menssagem;";
  //   $res = pg_exec($conn, $sql);
  //
  //   // Faz a leitura de cada linha recuperada do banco
  //   while($linha = pg_fetch_array($res,$row = NULL, $result_type = PGSQL_ASSOC)){
  //     $vetor[] = $linha;
  //   }
  //
  //   $response = $this->getResponseWithHeader()->setContent(json_encode($vetor));
  //   return $response;
  // }
  // public function update($id, $data){
  //   require_once 'public/Connection.php';
  //   $sql = "update menssagem SET id_turma = '".$data['id']."', menssagem = '".$data['menssagem']."' where id_menssagem = '".$id."';";
  //   $res = pg_exec($conn,$sql);
  //
  //   if(pg_last_error($conn) == ""){
  //     $resposta = true;
  //   } else {
  //     $resposta = pg_last_error($conn);
  //   }
  //
  //   $response = $this->getResponseWithHeader()
  //   ->setContent(json_encode($resposta));
  //   return $response;
  // }
  //
  // public function delete($id){
  //   require_once 'public/Connection.php';
  //   $sql = "delete from menssagem where id_menssagem = '".$id."';";
  //   $res = pg_exec($conn,$sql);
  //
  //   if(pg_result_status($res) == 1){
  //     $resposta = true;
  //   } else {
  //     $resposta = pg_last_error($conn);
  //   }
  //
  //   $response = $this->getResponseWithHeader()
  //   ->setContent(json_encode($resposta));
  //   return $response;
  // }


  public function includeHeader(){
    $response = $this->getResponse();
    $response->getHeaders()
    ->addHeaderLine('Access-Control-Allow-Origin','*')
    ->addHeaderLine('Access-Control-Allow-Methods','POST PUT DELETE GET');

    header('Content-Type: application/json;charset=UTF-8');
    return $response;
  }
}
