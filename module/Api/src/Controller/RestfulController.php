<?php

namespace Api\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;

class RestfulController extends AbstractRestfulController
{
    public function get($parametros)
    {
      $tabela = $parametros['tabela'];
      $id = $parametros['id'];

      require_once './public/Connection.php';
      if ($id!='*'){
        $sql = "select * from ".$tabela." where matricula='".$id."';";
      } else if ($id == '*'){
        $sql = "select * from ".$tabela.";";
      }

      $res = pg_exec($conn, $sql);

      for ($i=0;$i<6;$i++){
        $arrayComentarios[] = [
          'created_time' => 'Data da criação',
          'message' => 'Mensagens dos usuários '.($i+1)
        ];
      }

      if (!isset($parametros['tipo'])){
        while($linha = pg_fetch_array($res,$row = NULL, $result_type = PGSQL_ASSOC)){
          $vetor[] = $linha;
        }
      } else {
        while($linha = pg_fetch_array($res,$row = NULL, $result_type = PGSQL_ASSOC)){
          $infobd = array(
            'linkqr' => $linha['linkqr'],
            'legenda' => $linha['legenda'],
          );

          if ($linha['object_id']!='fixa'){
            $infoFacebook = pg_exec($conn, "select app_id,app_secret,default_graph_version from config");
            $resultado = pg_fetch_array($infoFacebook,$row = NULL, $result_type = PGSQL_ASSOC);

            $newFacebook = array(
              'app_id' => $resultado['app_id'], //id do aplicativo do SID
              'app_secret' => $resultado['app_secret'], // Senha do aplicativo SID
              'default_graph_version' => $resultado['default_graph_version'], // Versão da Graph API
            );

            $tokenInfinito = "EAACVVnZBeZA2YBAIWIZCr0VIerPBCX91NAgWZAzl0bpI2abJl47KWNSEYyEfZCKJPubjU33JAmXAdm5owZCJBZB0SbFCP8CH7PreM2DfG2gxLBm4Mv3wXmA6QnJ6hQ1ioefZCFqjIZCZCrV7hk9kMKNXpRj1RWsFj3irW23Q0UjwNwP7zZA9ZBEOL7wJy6RFtpehBSXW4w4qnRW3DAZDZD";
            $fb = new \Facebook\Facebook($newFacebook);
            try {
              $response = $fb->get(
                '/'.$linha['object_id'].'/comments',
                $tokenInfinito
              );
            } catch(FacebookExceptionsFacebookResponseException $e) {
              return 'Graph returned an error: ' . $e->getMessage();
              exit;
            } catch(FacebookExceptionsFacebookSDKException $e) {
              return 'Facebook SDK returned an error: ' . $e->getMessage();
              exit;
            }

            $comentarios = $response->getDecodedBody();
            for($i=0; $i<count($comentarios['data']); $i++){
              $arrayComentarios[]=$comentarios['data'][$i];
            }
          }

          // Cria um Array com as informaçoes recuperadas.
          $vetor[] = array(
            'bd' => $infobd, // Informaçoes do Banco
            'comentarios' => $arrayComentarios, // Informaçoes do Face
            'imagem' => base64_encode(file_get_contents("./public/imagens/".$linha['object_id'].".png")),
          );
        }
      }

    $response = $this->getResponseWithHeader()->setContent(json_encode($vetor));

    return $response;
    }

    public function getList(){
      require_once './public/Connection.php';
      $sql = "select TABLE_NAME from INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='public' ORDER BY TABLE_NAME;";
      $res = pg_exec($conn, $sql);

      while($linha = pg_fetch_array($res,$row = NULL, $result_type = PGSQL_ASSOC)){
        $vetor[] = $linha;
      }

      $response = $this->getResponseWithHeader()->setContent(json_encode($vetor));
      return $response;
    }

    public function create($parametros)
    {
      $tabela = $parametros['tabela'];

      require_once './public/Connection.php';

      if ($tabela=='turma'){
        $sql = "insert into turma values ('".$parametros['id']."');";
      } else if ($tabela=='mensagem'){
        $sql = "insert into ".$tabela." (id_professor,id_turma,mensagem) values ('".$parametros['id']."','".$parametros['id_turma']."','".$parametros['mensagem']."');";
      } else if ($tabela=='aluno'){
        $sql = "BEGIN TRANSACTION;
                insert into aluno values ('".$parametros['nome']."','".$parametros['matricula']."','".password_hash($parametros['senha'],PASSWORD_BCRYPT)."');
                insert into matriculado values ('".$parametros['matricula']."','".$parametros['turma']."');
                COMMIT TRANSACTION;
                ";
      } else if ($tabela=='professor'){
        $sql = "BEGIN TRANSACTION;
                insert into professor values ('".$parametros['nome']."','".$parametros['matricula']."','".password_hash($parametros['senha'],PASSWORD_BCRYPT)."');
                insert into designado values ('".$parametros['matricula']."','".$parametros['turma']."');
                COMMIT TRANSACTION;
                ";
      } else if ($tabela=='divulgacao'){
        $arquivo_tmp = $_FILES['imagem']['tmp_name'];// ARAMAZENA A IMAGEM INCLUIDA EM UM BUFFER
        $nome = $_FILES['imagem']['name'];
        $extensao = strrchr($nome, ".");
        $extensao = strtolower($extensao); // Converte a extensao para mimusculo
        $arquivo_destino = "./public/imagens/".$parametros['object_id'].$extensao;
        if(move_uploaded_file($arquivo_tmp, $arquivo_destino)){
          $sql = "insert into divulgacao (fbid,linkqr,legenda,object_id,datatermino,datainicio) values (".$parametros['fbid'].",'".$parametros['linkqr']."','".$parametros['legenda']."','".$parametros['object_id']."','".$parametros['datatermino']."','".$parametros['datatermino']."');";
        }
      }

      $res = pg_query($sql);

      if(pg_last_error($conn) == ""){
        $resposta = true;
      } else {
        $resposta = pg_last_error($conn);
      }

      $response = $this->getResponseWithHeader()->setContent(json_encode($resposta));

      return $response;
    }

    public function update($id,$data)
    {
      $tabela = $id['tabela'];

      require_once './public/Connection.php';
      if ($tabela=='1'){

      } else if ($tabela=='mensagem'){
        $matricula = $id['id'];
        $id_turma = $id['id_turma'];
        $mensagem = $id['mensagem'];
        $id_mensagem = $id['id_mensagem'];

        $sql = "update mensagem SET mensagem='".$mensagem."' WHERE id_mensagem=".$id_mensagem.";";
      } else if ($tabela=='aluno'){
        $matricula = $id['matricula'];
        $nome = $id['nome'];

        $sql = "update aluno SET nome='".$nome."' WHERE matricula='".$matricula."';";
      }else if ($tabela=='professor'){
        $matricula = $id['matricula'];
        $nome = $id['nome'];

        $sql = "update professor SET nome='".$nome."' WHERE matricula='".$matricula."';";
      }


      $res = pg_query($sql);

      if(pg_last_error($conn) == ""){
        $resposta = true;
      } else {
        $resposta = pg_last_error($conn);
      }

      $response = $this->getResponseWithHeader()->setContent(json_encode($resposta));

      return $response;
    }

    public function delete($parametros)
    {
      $tabela = $parametros['tabela'];

      require_once './public/Connection.php';

      if ($tabela=='aluno'){
        $matricula = $parametros['matricula'];
        $sql = "delete from ".$tabela." where matricula='".$matricula."';";
      } else if ($tabela=='professor'){
        $matricula = $parametros['matricula'];
        $sql = "delete from ".$tabela." where matricula='".$matricula."';";
      } else if ($tabela=='mensagem'){
        $id_mensagem = $parametros['id_mensagem'];
        $sql = "delete from ".$tabela." where id_mensagem=".$id_mensagem.";";
      } else if ($tabela=='turma'){
        $id = $parametros['id'];
        $sql = "delete from ".$tabela." where id='".$id."';";
      } else if ($tabela=='divulgacao'){
        $id = $parametros['divid'];
        $myFile = "./public/imagens/".$parametros['object_id'].".png";
        unlink($myFile) or die("Couldn't delete file");
        $sql = "delete from ".$tabela." where divid='".$id."';";
      }

      $res = pg_query($sql);

      if($res){
        $resposta = true;
      } else {
        $resposta = pg_last_error($conn);
      }

      $response = $this->getResponseWithHeader()->setContent(json_encode($resposta));

      return $response;
    }

    // configure response
    public function getResponseWithHeader()
    {
        $response = $this->getResponse();
        $response->getHeaders()
                 ->addHeaderLine('Access-Control-Allow-Origin','*')
                 ->addHeaderLine('Access-Control-Allow-Methods','POST PUT DELETE GET');

        return $response;
    }
}
