<?php

namespace Adm\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\ServiceManager\ServiceManager;

use Zend\Authentication\AuthenticationService;
use Adm\Auth\Adapter;

class AuthController extends AbstractActionController
{
  function logarAction(){
    session_start();
    $this->layout()->setTemplate('layout/layoutAuth');
    $authAdapter = new Adapter($_POST['usuario'], $_POST['senha']);
    $result = $authAdapter->authenticate();

    if (!$result->isValid()) {
      $_SESSION['loginErro'] = "Login ou senha Incorretos!";
      return $this->redirect ()->toRoute ('home', ['action' => 'index']);
    } else {
      $_SESSION['login'] = "ok";
      return $this->redirect ()->toRoute ( 'auth/administrador', array (
        'controller' => 'administrador',
        'action' => 'index'
      ));
    }
  }

  public function indexAction()
  {
    if (isset($_SESSION['loginErro'])){
      echo '<script>alert("'.$_SESSION['loginErro'].'");</script>';
      unset($_SESSION['loginErro']);
    }

    $this->layout()->setTemplate('layout/layoutAuth');

    $newFacebook = [
  		'app_id' => '164198740813670',
      'app_secret' => 'f435a5e76649e9d8673170639591b57d',
      'default_graph_version' => 'v3.3'
  	];

    $fb = new \Facebook\Facebook ($newFacebook);
    $helper = $fb->getRedirectLoginHelper();
    $permissions = [];

    $result = new ViewModel(array (
      'logarFacebook' => $helper->getReRequestUrl('http://'.$_SERVER['HTTP_HOST'].'/auth/callback', $permissions),
      'logarLocal' => 'auth/logar'
    ));

    return $result;
  }

  public function callbackAction()
  {
    session_start();
    $newFacebook = [
			'app_id' => '164198740813670',
      'app_secret' => 'f435a5e76649e9d8673170639591b57d',
			'default_graph_version' => 'v3.3',
		];

    $fb = new \Facebook\Facebook ( $newFacebook );
    $helper = $fb->getRedirectLoginHelper();

    try {
      $accessToken = $helper->getAccessToken();
    } catch ( Facebook\Exceptions\FacebookResponseException $e ) {
  		echo 'Graph returned an error: ' . $e->getMessage ();
  		exit ();
  	} catch ( Facebook\Exceptions\FacebookSDKException $e ) {
  		echo 'Facebook SDK returned an error: ' . $e->getMessage ();
  		exit ();
  	}

    if (isset($accessToken)) {
      $_SESSION['facebook_access'] = [ 'accessToken' => (string) $accessToken];
      $_SESSION['login'] = "ok";
      return $this->redirect ()->toRoute ( 'auth/administrador', array (
        'controller' => 'administrador',
        'action' => 'index'
      ));
    } elseif ($helper->getError()) {
      var_dump($helper->getError());
      var_dump($helper->getErrorCode());
      var_dump($helper->getErrorReason());
      var_dump($helper->getErrorDescription());
      exit;
    }

    http_response_code(400);
    exit;
  }
}
