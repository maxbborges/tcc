<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Api;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'restful' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/restful',
                    'defaults' => [
                        'controller' => Controller\RestfulController::class,
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                  'requisicoes' => [
                      'type' => Segment::class,
                      'options' => [
                          'route'    => '/',
                          'defaults' => [
                              'controller' => Controller\RequisicoesController::class,
                              'action'=> 'index',
                          ],
                      ],
                  ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\RequisicoesController::class => InvokableFactory::class,
            Controller\RestfulController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
