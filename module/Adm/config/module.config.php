<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Adm;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'index',
                    ],
                ],
              ],
              'auth' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/auth',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                  'adm' => [
                      'type' => Segment::class,
                      'options' => [
                          'route' => '/[:action]',
                            'defaults' => [
                              'controller' => Controller\AuthController::class,
                            ],
                        ],
                    ],
                  'administrador' => [
                      'type' => Segment::class,
                      'options' => [
                        'route' => '/administrador[/:action]',
                        'defaults' => [
                            'controller' => Controller\AdministradorController::class,
                        ],
                      ],
                  ],
                ],
            ],
            'add' => [
                'type' => Literal::class,
                'options' => [
                  'route' => 'addx',
                  'defaults' => [
                      'controller' => Controller\AdministradorController::class,
                      'action'     => 'index',
                  ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\AuthController::class => InvokableFactory::class,
            Controller\AdministradorController::class => InvokableFactory::class,
        ],
    ],
    'module_layouts' => array(
      'Adm' => 'adm',
      'Api' => 'api',
      'Web' => 'web'
    ),
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'adm'                     => __DIR__ . '/../../Adm/view/layout/layout.phtml',
            'api'                     => __DIR__ . '/../../Api/view/layout/layout.phtml',
            'web'                     => __DIR__ . '/../../Web/view/layout/layout.phtml',
            'adm/index/index' => __DIR__ . '/../view/adm/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
