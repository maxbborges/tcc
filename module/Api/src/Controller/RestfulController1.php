<?php

namespace Api\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\ServiceManager\ServiceManager;

class RestfulController extends AbstractActionController
{
  public function getAction(){
    return new ViewModel();
  }

  public function getListAction(){
    // Realiza a conexão com o banco e pega todas as colunas do banco.
    require_once 'public/Connection.php';

    $imagens = 'public/imagens/';
    // $numeroImagens = glob("$imagens{*.jpg,*.JPG,*.png,*.gif,*.bmp}", GLOB_BRACE);
    // $numeroDivulgacoes = pg_fetch_array((pg_exec($conn, "select count(*) from divulgacao;")),$row = NULL, $result_type = PGSQL_ASSOC);
    //
    // if(count($numeroImagens)>$numeroDivulgacoes['count']) {}
    $divulgacoes = pg_exec($conn, "select * from divulgacao order by divid");
    $infoFacebook = pg_exec($conn, "select app_id,app_secret,default_graph_version from config");
    $resultado = pg_fetch_array($infoFacebook,$row = NULL, $result_type = PGSQL_ASSOC);

    $newFacebook = array(
      'app_id' => $resultado['app_id'], //id do aplicativo do SID
      'app_secret' => $resultado['app_secret'], // Senha do aplicativo SID
      'default_graph_version' => $resultado['default_graph_version'], // Versão da Graph API
    );

    //PAGINA IFB:
    $tokenInfinito = "EAACVVnZBeZA2YBAOBLUU7gyzsLXuRbQUm1ofHJ6Xfj9wR7ZBxcTdgXR5mHwFXSB7MBiY1apBkRjkZAsQpGir7mSCbcWp9H0gWUEe3olIDvKkwbQqYEGegzlec24CYlwObDtAS5H1qJOWkenYz3jvqoJsczjUFAAZD";

    // Faz a leitura de cada linha recuperada das divulgacoes
    while($linha = pg_fetch_array($divulgacoes,$row = NULL, $result_type = PGSQL_ASSOC)){
      // Verifica a data de cada publicação!
      if(date('Y-m-d')<$linha['datatermino']&&date('Y-m-d')>$linha['datainicio']){
        // Verifica cada publicação. A posicao 0 é destinada a publicação fixa, não possuindo Comentarios.
        if ($linha['divid']!=0){

          // Realiza a conexão com a API do Facebook.
          $fb = new \Facebook\Facebook($newFacebook);

          // Recupera a imagem da publicação!
          $imagem = $fb->get('/'.$linha['object_id'].'?fields=images', $tokenInfinito);
          $imagem = $imagem->getDecodedBody();
          // echo $fb->get('/'.$linha['object_id'].'/comments', $tokenInfinito);

          // Recupera todos os comentarios da publicação, usando o object_id de cada divulgação.
          $comentarios = $fb->get('/'.$linha['object_id'].'/comments', $tokenInfinito);
          $comentarios = $comentarios->getDecodedBody();

          // Percorre todos os comentarios recuperados.
          for($i=0; $i<count($comentarios['data']); $i++){
            // Recupera todas os likes da publicação
            $likes = ($fb->get('/'.$comentarios['data'][$i]['id'].'/likes', $tokenInfinito));
            $likes = $likes->getDecodedBody();

            // Percorre todos os likes feitos na publicação
            for($cont=0;$cont<count($likes['data']);$cont++){
              $buscaAdms = "select fbid from adm";
              $administradores = pg_exec($conn, $buscaAdms);

              // Percorre todos os administradores.
              while($linhaAdm=pg_fetch_array($administradores)){
                //Verifica se possui curtida do administrador.
                if($likes['data'][$cont]['id']==$linhaAdm[0]){
                  // $urlFoto = ($fb->get("/".$comentarios['data'][$i]['from']['id']."/?fields=picture.type(large)", $tokenInfinito))->getDecodedBody();
                  $url['urlFoto'] = $urlFoto['picture']['data']['url'];
                  $arrayComentarios[] = array_merge($comentarios['data'][$i], $url);
                  break;
                }
              }

              if($arrayComentarios!=null){
                break;
              }
            }
          }
        }

        $infobd = array(
          'linkqr' => $linha['linkqr'],
          'legenda' => $linha['legenda'],
        );
        $arrayComentarios=null;
        // Cria um Array com as informaçoes recuperadas.
        $json[] = array(
          'bd' => $infobd, // Informaçoes do Banco
          'comentarios' => $arrayComentarios, // Informaçoes do Face
          'imagem' => base64_encode(file_get_contents("./public/imagens/".$linha['object_id'].".png")),
        );

      }
    }
    $json = json_encode($json); // Transforma o Array em JSON
    $response = $this->includeHeader()->setContent($json);
    return $response;
  }

  public function includeHeader(){
    $response = $this->getResponse();
    $response->getHeaders()
    ->addHeaderLine('Access-Control-Allow-Origin','*')
    ->addHeaderLine('Access-Control-Allow-Methods','POST PUT DELETE GET');

    header('Content-Type: application/json;charset=UTF-8');
    return $response;
  }
}
