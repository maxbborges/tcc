<?php

namespace Adm\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\ServiceManager\ServiceManager;

use Zend\Http\Client as HttpClient;

class AdministradorController extends AbstractActionController
{
  public function indexAction() {
    if(!isset($_SESSION['login'])){
        return $this->redirect()->toRoute('home');
    }

    return new ViewModel();
  }

  public function inserirAction() {
    if(!isset($_SESSION['login'])){
        return $this->redirect()->toRoute('home');
    }

    $client = new HttpClient();
    $client->setAdapter('Zend\Http\Client\Adapter\Curl');
    $tabelas = array('turma');

    for ($i=0;$i<count($tabelas);$i++){
      $parametros = [
        'tabela'=>$tabelas[$i],
        'id'=>'*',
      ];

      $response = $client
        ->setUri('http://localhost:80/restful/')
        ->setMethod('GET')
        ->setParameterGET($parametros)
        ->send();

      $tudo[] = json_decode($response->getBody());

    }

    return new ViewModel(['response'=>$tudo]);
  }

  public function listarAction() {
    if(!isset($_SESSION['login'])){
        return $this->redirect()->toRoute('home');
    }

    $lista = $this->params()->fromQuery();
    $client = new HttpClient();
    $client->setAdapter('Zend\Http\Client\Adapter\Curl');
    $tabelas = array('aluno','professor','turma','divulgacao');

    for ($i=0;$i<count($tabelas);$i++){
      $parametros = [
        'tabela'=>$tabelas[$i],
        'id'=>'*',
      ];

      $response = $client
        ->setUri('http://localhost:80/restful/')
        ->setMethod('GET')
        ->setParameterGET($parametros)
        ->send();

      $response = json_decode($response->getBody());

      $tudo[] = $response;

    }
    return new ViewModel(['response'=>$tudo,'lista'=>$lista]);
  }

  public function modificarAction() {
    if(!isset($_SESSION['login'])){
        return $this->redirect()->toRoute('home');
    }
  }

  public function removerAction() {
    if(!isset($_SESSION['login'])){
        return $this->redirect()->toRoute('home');
    }

    $parametros = $this->params()->fromQuery();
    if ($parametros==NULL){
      return new ViewModel();
    } else {
      $client = new HttpClient();
      $client->setAdapter('Zend\Http\Client\Adapter\Curl');

      if ($parametros == NULL){
        $parametros = [
          'tabela'=>'aluno',
          'matricula'=>1,
        ];
      }

      $response = $client
        ->setUri('http://localhost:80/restful/')
        ->setMethod('delete')
        ->setParameterGET($parametros)
        ->send();

      return $this->redirect()->toRoute('auth/administrador', ['action' => 'listar'], ['query' => ['lista'=>$parametros['tabela']]]);
    }
  }

  public function sairAction() {
    session_destroy();
    return $this->redirect()->toRoute('home', ['action' => 'index']);
  }
}
