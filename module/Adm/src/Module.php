<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Adm;


use Zend\ServiceManager\ServiceManager;

use Zend\Http\Client as HttpClient;

use Zend\Session\Validator\HttpUserAgent;
use Zend\Session\SessionManager;





use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\Router\RouteMatch;

use Zend\ModuleManager\ModuleManager;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\Redirect;
use Zend\Mvc\Controller\Plugin\Url;
use Zend\EventManager\EventManager;
use Zend\Http\Response;

class Module
{
    const VERSION = '3.0.3-dev';



    public function onBootstrap(MvcEvent $e)
    {
      session_start();
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $application = $e->getApplication();
        $serviceManager = $application->getServiceManager();
        $sessionManager = $serviceManager->get(SessionManager::class);



        $e->getApplication()->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e)
        {
          $controller = $e->getTarget();
          $controllerClass = get_class($controller);
          $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
          $config = $e->getApplication()->getServiceManager()->get('config');
          if (isset($config['module_layouts'][$moduleNamespace])) {
            $controller->layout($config['module_layouts'][$moduleNamespace]);
          }
        }
        , 100);
     }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
